# railtour

`railtour` allows to generate cart rail routes.

## usage

Near (up to 5 blocks distance) a non-air structure, `/roundtrip` generates
a cart rail roundtrip route around that structure.
