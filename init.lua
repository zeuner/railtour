local modname = "railtour"

local flat_plane = function (
    y
)
    return {
        get_point = function (
            self,
            x,
            z
        )
            return {
                x = x,
                y = y,
                z = z,
            }
        end,
        get_near = function (
            self,
            pos
        )
            return pos.x, pos.z
        end,
    }
end

local checkers_plane = function (
    y
)
    return {
        get_point = function (
            self,
            x,
            z
        )
            return {
                x = x,
                y = y + (
                    math.floor(
                        x / 3.0
                    ) + math.floor(
                        z / 3.0
                    )
                ) % 2,
                z = z,
            }
        end,
        get_near = function (
            self,
            pos
        )
            return pos.x, pos.z
        end,
    }
end

local get_square_border = function (
    min_x,
    min_z,
    max_x,
    max_z
)
    local result = {
    }
    for x = min_x, max_x - 1, 1 do
        result[
            #result + 1
        ] = {
            x,
            min_z,
        }
    end
    for z = min_z, max_z - 1, 1 do
        result[
            #result + 1
        ] = {
            max_x,
            z,
        }
    end
    for x = max_x, min_x + 1, -1 do
        result[
            #result + 1
        ] = {
            x,
            max_z,
        }
    end
    for z = max_z, min_z + 1, -1 do
        result[
            #result + 1
        ] = {
            min_x,
            z,
        }
    end
    return result
end

local apply_plane = function (
    plane,
    flat
)
    local result = {
    }
    for index, point in pairs(
        flat
    ) do
        result[
            index
        ] = plane:get_point(
            point[
                1
            ],
            point[
                2
            ]
        )
    end
    return result
end

local get_bounding_box = function (
    points
)
    local min = {
        x = math.huge,
        y = math.huge,
        z = math.huge,
    }
    local max = {
        x = -math.huge,
        y = -math.huge,
        z = -math.huge,
    }
    for _, point in pairs(
        points
    ) do
        for k, v in pairs(
            point
        ) do
            min[
                k
            ] = math.min(
                v,
                min[
                    k
                ]
            )
            max[
                k
            ] = math.max(
                v,
                max[
                    k
                ]
            )
        end
    end
    return min, max
end

local get_mapdata = function (
    manipulator,
    points
)
    local result = {
    }
    local min, max = get_bounding_box(
        points
    )
    local pmin, pmax = manipulator:read_from_map(
        min,
        max
    )
    local area = VoxelArea(
        pmin,
        pmax
    )
    local map_data = manipulator:get_data(
    )
    for index, point in pairs(
        points
    ) do
        result[
            index
        ] = map_data[
            area:indexp(
                point
            )
        ]
    end
    return result
end

local get_loose_roundtrip = function (
    manipulator,
    plane,
    x,
    z
)
    local distance = 5
    local min_x = x - distance
    local min_z = z - distance
    local max_x = x + distance
    local max_z = z + distance
    local flat = get_square_border(
        min_x,
        min_z,
        max_x,
        max_z
    )
    local points = apply_plane(
        plane,
        flat
    )
    local mapdata = get_mapdata(
        manipulator,
        points
    )
    local air = minetest.get_content_id(
        "air"
    )
    local no_air = false
    local at_min_x = false
    local at_min_z = false
    local at_max_x = false
    local at_max_z = false
    for index, content in pairs(
        mapdata
    ) do
        if air ~= content then
            no_air = true
            if min_x == flat[
                index
            ][
                1
            ] then
                at_min_x = true
            end
            if max_x == flat[
                index
            ][
                1
            ] then
                at_max_x = true
            end
            if min_z == flat[
                index
            ][
                2
            ] then
                at_min_z = true
            end
            if max_z == flat[
                index
            ][
                2
            ] then
                at_max_z = true
            end
        end
    end
    if not no_air then
        return nil
    end
    while true do
        no_air = false
        if at_min_x then
            min_x = min_x - distance
            at_min_x = false
        end
        if at_max_x then
            max_x = max_x + distance
            at_max_x = false
        end
        if at_min_z then
            min_z = min_z - distance
            at_min_z = false
        end
        if at_max_z then
            max_z = max_z + distance
            at_max_z = false
        end
        flat = get_square_border(
            min_x,
            min_z,
            max_x,
            max_z
        )
        points = apply_plane(
            plane,
            flat
        )
        mapdata = get_mapdata(
            manipulator,
            points
        )
        for index, content in pairs(
            mapdata
        ) do
            if air ~= content then
                no_air = true
                if min_x == flat[
                    index
                ][
                    1
                ] then
                    at_min_x = true
                end
                if max_x == flat[
                    index
                ][
                    1
                ] then
                    at_max_x = true
                end
                if min_z == flat[
                    index
                ][
                    2
                ] then
                    at_min_z = true
                end
                if max_z == flat[
                    index
                ][
                    2
                ] then
                    at_max_z = true
                end
            end
        end
        if not no_air then
            break
        end
    end
    return flat, min_x, min_z, max_x, max_z
end

local key_2d = function (
    value
)
    local key = ""
    for k, v in pairs(
        value
    ) do
        key = key .. "|" .. k .. "|" .. v
    end
    return key
end

local sequential_to_keyed = function (
    sequential
)
    local result = {
    }
    for index, value in pairs(
        sequential
    ) do
        result[
            key_2d(
                value
            )
        ] = value
    end
    return result
end

local von_neumann_neighbourhood_2d = {
}

for _, axis in pairs(
    {
        1,
        2,
    }
) do
    local minus = {
        0,
        0,
    }
    minus[
        axis
    ] = -1
    von_neumann_neighbourhood_2d[
        #von_neumann_neighbourhood_2d + 1
    ] = minus
    local plus = {
        0,
        0,
    }
    plus[
        axis
    ] = 1
    von_neumann_neighbourhood_2d[
        #von_neumann_neighbourhood_2d + 1
    ] = plus
end

local fill_air = function (
    manipulator,
    plane,
    keyed,
    min_x,
    max_x,
    min_z,
    max_z,
    min_points,
    max_points
)
    local air = minetest.get_content_id(
        "air"
    )
    local pmin, pmax = manipulator:read_from_map(
        min_points,
        max_points
    )
    local area = VoxelArea(
        pmin,
        pmax
    )
    local map_data = manipulator:get_data(
    )
    local done = {
    }
    while true do
        local found = false
        for flat_k, flat_v in pairs(
            keyed
        ) do
            found = true
            for _, offset in pairs(
                von_neumann_neighbourhood_2d
            ) do
                local neighbour = {
                    flat_v[
                        1
                    ] + offset[
                        1
                    ],
                    flat_v[
                        2
                    ] + offset[
                        2
                    ],
                }
                while true do
                    if min_x > neighbour[
                        1
                    ] then
                        break
                    end
                    if max_x < neighbour[
                        1
                    ] then
                        break
                    end
                    if min_z > neighbour[
                        2
                    ] then
                        break
                    end
                    if max_z < neighbour[
                        2
                    ] then
                        break
                    end
                    local neighbour_key = key_2d(
                        neighbour
                    )
                    if done[
                        neighbour_key
                    ] then
                        break
                    end
                    if keyed[
                        neighbour_key
                    ] then
                        break
                    end
                    local p = plane:get_point(
                        neighbour[
                            1
                        ],
                        neighbour[
                            2
                        ]
                    )
                    if p.x < pmin.x or p.y < pmin.y or p.z < pmin.z then
                        return nil
                    end
                    if p.x > pmax.x or p.y > pmax.y or p.z > pmax.z then
                        return nil
                    end
                    if air ~= map_data[
                        area:indexp(
                            p
                        )
                    ] then
                        break
                    end
                    keyed[
                        neighbour_key
                    ] = neighbour
                    break
                end
            end
            done[
                flat_k
            ] = flat_v
            keyed[
                flat_k
            ] = nil
            break
        end
        if not found then
            break
        end
    end
    return done
end

local moore_neighbourhood_2d = {
}

for x = -1, 1 do
    for z = -1, 1 do
        if 0 ~= x or 0 ~= z then
            moore_neighbourhood_2d[
                #moore_neighbourhood_2d + 1
            ] = {
                x,
                z,
            }
        end
    end
end

local tighten = function (
    manipulator,
    plane,
    keyed,
    min_x,
    max_x,
    min_z,
    max_z,
    min_points,
    max_points
)
    local hull = {
    }
    local filled = fill_air(
        manipulator,
        plane,
        keyed,
        min_x,
        max_x,
        min_z,
        max_z,
        min_points,
        max_points
    )
    for flat_k, flat_v in pairs(
        filled
    ) do
        local found = false
        for _, offset in pairs(
            moore_neighbourhood_2d
        ) do
            local neighbour = {
                flat_v[
                    1
                ] + offset[
                    1
                ],
                flat_v[
                    2
                ] + offset[
                    2
                ],
            }
            while true do
                if min_x > neighbour[
                    1
                ] then
                    break
                end
                if max_x < neighbour[
                    1
                ] then
                    break
                end
                if min_z > neighbour[
                    2
                ] then
                    break
                end
                if max_z < neighbour[
                    2
                ] then
                    break
                end
                local neighbour_key = key_2d(
                    neighbour
                )
                if filled[
                    neighbour_key
                ] then
                    break
                end
                found = true
                break
            end
            if found then
                break
            end
        end
        if found then
            hull[
                flat_k
            ] = flat_v
        end
    end
    return hull
end

local widen = function (
    manipulator,
    plane,
    keyed,
    min_x,
    max_x,
    min_z,
    max_z,
    min_points,
    max_points
)
    local wider = {
    }
    local air = minetest.get_content_id(
        "air"
    )
    local pmin, pmax = manipulator:read_from_map(
        min_points,
        max_points
    )
    local area = VoxelArea(
        pmin,
        pmax
    )
    local map_data = manipulator:get_data(
    )
    for flat_k, flat_v in pairs(
        keyed
    ) do
        for _, offset in pairs(
            moore_neighbourhood_2d
        ) do
            local neighbour = {
                flat_v[
                    1
                ] + offset[
                    1
                ],
                flat_v[
                    2
                ] + offset[
                    2
                ],
            }
            while true do
                if min_x > neighbour[
                    1
                ] then
                    break
                end
                if max_x < neighbour[
                    1
                ] then
                    break
                end
                if min_z > neighbour[
                    2
                ] then
                    break
                end
                if max_z < neighbour[
                    2
                ] then
                    break
                end
                local neighbour_key = key_2d(
                    neighbour
                )
                if wider[
                    neighbour_key
                ] then
                    break
                end
                local p = plane:get_point(
                    neighbour[
                        1
                    ],
                    neighbour[
                        2
                    ]
                )
                if p.x < pmin.x or p.y < pmin.y or p.z < pmin.z then
                    return nil
                end
                if p.x > pmax.x or p.y > pmax.y or p.z > pmax.z then
                    return nil
                end
                if air ~= map_data[
                    area:indexp(
                        p
                    )
                ] then
                    break
                end
                wider[
                    neighbour_key
                ] = neighbour
                break
            end
        end
        wider[
            flat_k
        ] = flat_v
    end
    return wider
end

local rail_neighbourhood_3d = {
}

for y = -1, 1, 1 do
    for _, axis in pairs(
        {
            "x",
            "z",
        }
    ) do
        local minus = {
            x = 0,
            y = y,
            z = 0,
        }
        minus[
            axis
        ] = -1
        rail_neighbourhood_3d[
            #rail_neighbourhood_3d + 1
        ] = minus
        local plus = {
            x = 0,
            y = y,
            z = 0,
        }
        plus[
            axis
        ] = 1
        rail_neighbourhood_3d[
            #rail_neighbourhood_3d + 1
        ] = plus
    end
end

local key_3d = function (
    value
)
    return value.x .. "|" .. value.y .. "|" .. value.z
end

local rail_sequence_possible = function (
    before,
    current,
    after
)
    if current.y < math.max(
        before.y,
        after.y
    ) then
        local dd = vector.subtract(
            vector.subtract(
                before,
                current
            ),
            vector.subtract(
                current,
                after
            )
        )
        if 0 ~= dd.x then
            return false
        end
        if 0 ~= dd.z then
            return false
        end
    end
    return true
end

local intersecting_path = function (
    one,
    other,
    depth,
    sequential
)
    local one_keys = {
    }
    local checking = depth
    local current = one
    while true do
        one_keys[
            current
        ] = true
        local next_entry = sequential[
            current
        ]
        if 0 == next_entry.generation then
            break
        end
        current = next_entry.entry.last
        checking = checking - 1
        if 0 >= checking then
            break
        end
    end
    local other_keys = {
    }
    local checking = depth
    local current = other
    while true do
        other_keys[
            current
        ] = true
        local next_entry = sequential[
            current
        ]
        if 0 == next_entry.generation then
            break
        end
        current = next_entry.entry.last
        checking = checking - 1
        if 0 >= checking then
            break
        end
    end
    for key, _ in pairs(
        one_keys
    ) do
        if other_keys[
            key
        ] then
            return true
        end
    end
    for key, _ in pairs(
        other_keys
    ) do
        if one_keys[
            key
        ] then
            return true
        end
    end
    return false
end

local shorten = function (
    roundtrip
)
    local remove_next
    for index, position in pairs(
        roundtrip
    ) do
        local candidate = roundtrip[
            (
                index + 2
            ) % #roundtrip + 1
        ]
        local found = false
        for _, offset in pairs(
            rail_neighbourhood_3d
        ) do
            if vector.equals(
                position,
                vector.add(
                    candidate,
                    offset
                )
            ) then
                local before_current = roundtrip[
                    (
                        index + #roundtrip - 2
                    ) % #roundtrip + 1
                ]
                local after_candidate = roundtrip[
                    (
                        index + 3
                    ) % #roundtrip + 1
                ]
                if rail_sequence_possible(
                    before_current,
                    position,
                    candidate
                ) and rail_sequence_possible(
                    position,
                    candidate,
                    after_candidate
                ) then
                    remove_next = index
                    found = true
                    break
                end
            end
        end
        if found then
            break
        end
    end
    if not found then
        return roundtrip, false
    end
    local shortened = {
    }
    for index, position in pairs(
        roundtrip
    ) do
        while true do
            if position % #roundtrip == (
                remove_next + 1
            ) % #roundtrip then
                break
            end
            if position % #roundtrip == (
                remove_next + 2
            ) % #roundtrip then
                break
            end
            shortened[
                #shortened + 1
            ] = position
            break
        end
    end
    return shortened, true
end

local sequentialize_roundtrip = function (
    plane,
    flat
)
    local points = {
    }
    for _, point_flat in pairs(
        flat
    ) do
        local point = plane:get_point(
            point_flat[
                1
            ],
            point_flat[
                2
            ]
        )
        points[
            key_3d(
                point
            )
        ] = point
    end
    local sequential = {
    }
    local this_generation = {
    }
    local generation = 0
    local start
    for key, value in pairs(
        points
    ) do
        this_generation[
            #this_generation + 1
        ] = {
            key = key,
            value = value,
        }
        start = value
        break
    end
    while true do
        local next_generation = {
        }
        local last = {
        }
        local sorted = {
        }
        for _, entry in pairs(
            this_generation
        ) do
            sorted[
                #sorted + 1
            ] = entry
        end
        table.sort(
            sorted,
            function (
                before,
                after
            )
                return vector.distance(
                    start,
                    before.value
                ) > vector.distance(
                    start,
                    after.value
                )
            end
        )
        this_generation = sorted
        for _, entry in pairs(
            this_generation
        ) do
            last[
                entry.key
            ] = points[
                entry.key
            ]
            points[
                entry.key
            ] = nil
        end
        for _, entry in pairs(
            this_generation
        ) do
            sequential[
                entry.key
            ] = {
                generation = generation,
                entry = entry,
            }
            local point = last[
                entry.key
            ]
            local neighbours = {
            }
            for _, offset in pairs(
                rail_neighbourhood_3d
            ) do
                local neighbour = vector.add(
                    point,
                    offset
                )
                neighbours[
                    #neighbours + 1
                ] = neighbour
            end
            table.sort(
                neighbours,
                function (
                    before,
                    after
                )
                    return vector.distance(
                        start,
                        before
                    ) > vector.distance(
                        start,
                        after
                    )
                end
            )
            for _, neighbour in pairs(
                neighbours
            ) do
                local neighbour_key = key_3d(
                    neighbour
                )
                local prelast
                if entry.last then
                    prelast = sequential[
                        entry.last
                    ]
                end
                while true do
                    if not points[
                        neighbour_key
                    ] then
                        break
                    end
                    if prelast then
                        if not rail_sequence_possible(
                            prelast.entry.value,
                            entry.value,
                            neighbour
                        ) then
                            break
                        end
                    end
                    next_generation[
                        neighbour_key
                    ] = {
                        key = neighbour_key,
                        value = neighbour,
                        last = entry.key,
                    }
                    break
                end
            end
        end
        if nil == next(
            next_generation
        ) then
            break
        end
        this_generation = next_generation
        generation = generation + 1
    end
    for k, v in pairs(
        this_generation
    ) do
        local last = v.value
        for _, offset in pairs(
            rail_neighbourhood_3d
        ) do
            local neighbour = vector.add(
                last,
                offset
            )
            local neighbour_key = key_3d(
                neighbour
            )
            while true do
                if v.key == neighbour_key then
                    break
                end
                if v.last == neighbour_key then
                    break
                end
                if not sequential[
                    neighbour_key
                ] then
                    break
                end
                if intersecting_path(
                    v.key,
                    neighbour_key,
                    5,
                    sequential
                ) then
                    break
                end
                if not rail_sequence_possible(
                    sequential[
                        v.last
                    ].entry.value,
                    v.value,
                    sequential[
                        neighbour_key
                    ].entry.value
                ) then
                    break
                end
                local subsequent = sequential[
                    neighbour_key
                ].entry.last
                if not rail_sequence_possible(
                    v.value,
                    sequential[
                        neighbour_key
                    ].entry.value,
                    sequential[
                        subsequent
                    ].entry.value
                ) then
                    break
                end
                local forward = {
                }
                local current = v.key
                while true do
                    local entry = sequential[
                        current
                    ]
                    forward[
                        #forward + 1
                    ] = entry.entry.value
                    current = entry.entry.last
                    if 0 == entry.generation then
                        break
                    end
                end
                local backward = {
                }
                current = neighbour_key
                while true do
                    local entry = sequential[
                        current
                    ]
                    backward[
                        #backward + 1
                    ] = entry.entry.value
                    current = entry.entry.last
                    if 1 == entry.generation then
                        break
                    end
                end
                for i = #backward, 1, -1 do
                    forward[
                        #forward + 1
                    ] = backward[
                        i
                    ]
                end
                while true do
                    local shortened
                    forward, shortened = shorten(
                        forward
                    )
                    if not shortened then
                        break
                    end
                end
                return forward
            end
        end
    end
end

minetest.register_chatcommand(
    "roundtrip",
    {
        params = "",
        description = "Place rails around an object",
        privs = {
            server = true,
        },
        func = function (
            player_name
        )
            local player = minetest.get_player_by_name(
                player_name
            )
            local base_pos = player:get_pos(
            )
            for k, v in pairs(
                base_pos
            ) do
                base_pos[
                    k
                ] = math.floor(
                    v + 0.5
                )
            end
            local plane = checkers_plane(
                base_pos.y
            )
            local x, z = plane:get_near(
                base_pos
            )
            local manipulator = minetest.get_voxel_manip(
            )
            if not manipulator then
                minetest.chat_send_player(
                    player_name,
                    modname .. ": could not initialize voxel manipulator"
                )
                return
            end
            local flat, min_x, min_z, max_x, max_z = get_loose_roundtrip(
                manipulator,
                plane,
                x,
                z
            )
            if not flat then
                minetest.chat_send_player(
                    player_name,
                    modname .. ": could not compute roundtrip"
                )
                return
            end
            local keyed = sequential_to_keyed(
                flat
            )
            local points = apply_plane(
                plane,
                keyed
            )
            local min, max = get_bounding_box(
                points
            )
            local hull = tighten(
                manipulator,
                plane,
                keyed,
                min_x,
                max_x,
                min_z,
                max_z,
                min,
                max
            )
            if not hull then
                minetest.chat_send_player(
                    player_name,
                    modname .. ": could not tighten roundtrip"
                )
                return
            end
            hull = widen(
                manipulator,
                plane,
                hull,
                min_x,
                max_x,
                min_z,
                max_z,
                min,
                max
            )
            hull = widen(
                manipulator,
                plane,
                hull,
                min_x,
                max_x,
                min_z,
                max_z,
                min,
                max
            )
            hull = widen(
                manipulator,
                plane,
                hull,
                min_x,
                max_x,
                min_z,
                max_z,
                min,
                max
            )
            hull = widen(
                manipulator,
                plane,
                hull,
                min_x,
                max_x,
                min_z,
                max_z,
                min,
                max
            )
            points = sequentialize_roundtrip(
                plane,
                hull
            )
            if not points then
                minetest.chat_send_player(
                    player_name,
                    modname .. ": could not find roundtrip sequence"
                )
                return
            end
            local pmin, pmax = manipulator:read_from_map(
                min,
                max
            )
            local area = VoxelArea(
                pmin,
                pmax
            )
            local map_data = manipulator:get_data(
            )
            local rail = minetest.get_content_id(
                "carts:rail"
            )
            local powerrail = minetest.get_content_id(
                "carts:powerrail"
            )
            local brakerail = minetest.get_content_id(
                "carts:brakerail"
            )
            local stop_area = 12
            local regular_steps = 0
            for index, point in pairs(
                points
            ) do
                local previous_point = points[
                    (
                        index + #points - 2
                    ) % #points + 1
                ]
                local next_point = points[
                    index % #points + 1
                ]
                local current_rail
                if 0 < stop_area then
                    if previous_point.y ~= point.y then
                        current_rail = rail
                    elseif next_point.y ~= point.y then
                        current_rail = rail
                    else
                        stop_area = stop_area - 1
                        current_rail = brakerail
                    end
                elseif 5 < regular_steps then
                    regular_steps = 0
                    current_rail = powerrail
                else
                    regular_steps = regular_steps + 1
                    current_rail = rail
                end
                map_data[
                    area:indexp(
                        point
                    )
                ] = current_rail
            end
            manipulator:set_data(
                map_data
            )
            manipulator:calc_lighting(
            )
            manipulator:update_liquids(
            )
            manipulator:write_to_map(
            )
        end,
    }
)
